if (!("ontouchstart" in  window)) {
	document.documentElement.className += " no-touch";
} else {
	document.documentElement.className += " touch";
}

$(document).ready( function() {
    if($('.js-styled').length) {
        $('.js-styled').styler({
            selectSmartPositioning : false
        });
    };
    
    $(".select-image").msDropdown({visibleRows:10});
    

    $(".fancybox").fancybox({
         onComplete : function() {
                 $('.window-open .type-room').on('click touchstart',function(event) {
                    $.fancybox.close();
                    
                })
            }
    });
    
    $('.window-open .type-room').on('click touchstast', function () {
        $.fancybox.close();
        return false;
    });
     

    // Переключение кнопок для количества персон
    $('.number-minus').click(function () {
        var $input = $(this).parents('.number').find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.number-plus').click(function () {
        var $input = $(this).parents('.number').find('input');
        var count = parseInt($input.val()) + 1;
        count = count > ($input.attr("maxlength")) ? ($input.attr("maxlength")) : count;
        $input.val(count);
        $input.change();
        return false;
    });
});

$(function(){
	/*placeholder*/
    $('input, textarea').each(function(){
 		var placeholder = $(this).attr('placeholder');
 		$(this).focus(function(){ $(this).attr('placeholder', '');});
 		$(this).focusout(function(){			 
 			$(this).attr('placeholder', placeholder);  			
 		});
 	});
	/* placeholder*/
    
    /* index header slider*/
    if($('.header-slider').length){
    	$('.header-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
			dots:true,
			autoplay:true,
            fade:true,
			appendArrows: $(".header-slider__controls"),
            appendDots: $(".header-slider__controls"),
             asNavFor: '.header-image-slider',
             focusOnSelect: true,
            speed:600, 
            autoplaySpeed:6000
    	});
        
        $('.header-image-slider').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          dots: false,
          fade: true,
          asNavFor: '.header-slider',
          speed:900, 
            autoplaySpeed:6000
        });
        
    }; 
    /* index header slider*/

    
    /* index slider-room-images*/
    if($('.slider-room-images').length){
    	$('.slider-room-images').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
			dots:false,
            arrows:true,
            infinite:false,
           // variableWidth:true,
            appendArrows: $(".slider-room-images__controls"),
            responsive: [
            {
              breakpoint: 1200,
              settings: {
                 slidesToShow: 5,
              }
            }, {
              breakpoint: 1000,
              settings: {
                 slidesToShow: 4,
              }
            }, {
              breakpoint: 768,
              settings: {
                 slidesToShow: 3,
              }
            }, {
              breakpoint: 639,
              settings: {
                 slidesToShow: 2,
              }
            }]
    	});
    }; 
    /* index slider-room-images*/
    
    /* index slider-info*/
    $('.slider-info').each(function() {
        $(this).slick({
            slidesToShow: 1,
            slidesToScroll: 1,
			dots:true,
            fade:true,
            draggable:false,
            appendArrows:$(this).parent().find(".slider-info__controls"),
            appendDots:$(this).parent().find('.slider-info__controls')
        });
    })
    
    /* index slider-info*/
    
    /* index slider-medicine-center*/
    if($('.slider-medicine-center').length){
    	$('.slider-medicine-center').slick({
            slidesToShow: 4,
            slidesToScroll: 4,
			dots:true,
            arrows:true,
            vertical:true,
            appendDots: $(".slider-medicine-center__controls"),
    	});
    }; 
    /* index slider-medicine-center*/
    
    /* fancybox gallery*/
    $('[data-fancybox]').fancybox({        
        margin : [ 40, 0, 104, 0 ],
        infobar : true,
        buttons : true,
        // What buttons should appear in the toolbar
        slideShow  : false,
        fullScreen : false,
        thumbs     : false,
        closeBtn   : true,
        closeClickOutside : true,
        touch : true,
        thumbs : {
            showOnStart : true
        },        
        
            onComplete : function() {
                 $('.fancybox-slide').on('click touchstart',function(event) {
                    if (!$(event.target).closest('.fancybox-placeholder').length) {
                        $.fancybox.close();
                    } 
                })
            }
        

    })
    /* fancybox gallery*/
  
    
    
    /* img to background-image block-mineral-category-image*/
    $('.block-mineral-category-image').each(function(index, element) {
        var image=$(this).attr('src');
		$(this).parent('.block-mineral-category__image').css('background-image', 'url('+image+')');
		$(this).remove();
    });
    /* img to background-image block-mineral-category-image*/
    
    
    /* slider-bottom*/
    $('.slider-bottom').each(function() {
        $(this).slick({
            slidesToShow: 1,
            slidesToScroll: 1,
			dots:true,
            fade:true,
            draggable:false,
            autoplay:true,
            appendArrows:$(this).parent().find(".slider-bottom__controls"),
            appendDots:$(this).parent().find('.slider-bottom__controls')
        });
    })
     /* slider-bottom*/
    
    
    
    /*---------------google map-------------------*/
	if ($('#map').length) {
		
		var styles= [
		  {
			featureType: "all",
			stylers: [
			  { saturation: -100 }
			]
		  }
		];
		 var styledMap = new google.maps.StyledMapType(styles,
   		 {name: "Styled Map"});
		
	    google.maps.event.addDomListener(window, 'load', initMap);

	    function initMap() {
	        var map = new google.maps.Map(document.getElementById('map'), {
	            zoom: 12,
	            scrollwheel: false,
	            center: {
	                 lat: 44.0531216,
	                lng: 42.8551456,
	            }
	        });

	        var image = 'images/icon-map-marker.png';
	        var beachMarker = new google.maps.Marker({
	            position: {
	                 lat: 44.0531216,
	                lng: 42.8551456,
	            },
	            map: map,
	            icon: image
	        });
			
			 map.mapTypes.set('map_style', styledMap);
 			 map.setMapTypeId('map_style');
			
	    }
	};
	/*---------------google map-------------------*/
    
   
    
    /* ------------------- parallax ------------------ */
    parallaxScroll();
    $(window).bind('scroll', function(e) {
        parallaxScroll();
    });

    function parallaxScroll() {
        console.log($('.parallax-wrap').offset().top);
        
        if($('.parallax-wrap-1').length){
        
            var scrolled = $(window).scrollTop()-$('.parallax-wrap-1').offset().top;
            $('#parallax-bg1').css('top', (0 - (scrolled * .8)) + 'px');
        }
        
        if($('.parallax-wrap-2').length){
            var scrolled = $(window).scrollTop()-$('.parallax-wrap-2').offset().top;
            $('#parallax-bg2').css('top', (0 - (scrolled * .4)) + 'px');
        }
        
        if($('.parallax-wrap-4').length){
            var scrolled = $(window).scrollTop()-$('.parallax-wrap-4').offset().top;
            $('#parallax-bg4').css('top', (0 - (scrolled * .4)) + 'px');
        }
        
        if($('.parallax-wrap-about-2').length){
            var scrolled = $(window).scrollTop()-$('.parallax-wrap-about-2').offset().top;
            $('#parallax-bg-about-2').css('top', (0 - (scrolled * .4)) + 'px');
        }
        if($('.parallax-wrap-about-3').length){
            var scrolled = $(window).scrollTop()-$('.parallax-wrap-about-3').offset().top;
            $('#parallax-bg-about-3').css('top', (0 - (scrolled * .4)) + 'px');
        }
        
        if($('.parallax-wrap-active-1').length){
            var scrolled = $(window).scrollTop()-$('.parallax-wrap-active-1').offset().top;
            $('#parallax-bg-active-1').css('top', (0 - (scrolled * .4)) + 'px');
        }
        
        if($('.parallax-wrap-food-1').length){
            var scrolled = $(window).scrollTop()-$('.parallax-wrap-food-1').offset().top;
            $('#parallax-bg-food-1').css('top', (0 - (scrolled * .4)) + 'px');
        }
        if($('.parallax-wrap-food-view-1').length){
            var scrolled = $(window).scrollTop()-$('.parallax-wrap-food-view-1').offset().top;
            $('#parallax-bg-food-view-1').css('top', (0 - (scrolled * .4)) + 'px');
        }
        
        if($('.parallax-wrap-gallery-1').length){
            var scrolled = $(window).scrollTop()-$('.parallax-wrap-gallery-1').offset().top;
            $('#parallax-bg-gallery-1').css('top', (0 - (scrolled * .4)) + 'px');
        }
        
        if($('.parallax-wrap-med-1').length){
            var scrolled = $(window).scrollTop()-$('.parallax-wrap-med-1').offset().top;
            $('#parallax-bg-med-1').css('top', (0 - (scrolled * .4)) + 'px');
        }
        
        if($('.parallax-wrap-news-1').length){
            var scrolled = $(window).scrollTop()-$('.parallax-wrap-news-1').offset().top;
            $('#parallax-bg-news-1').css('top', (0 - (scrolled * .4)) + 'px');
        }
        
        if($('.parallax-wrap-prices-1').length){
            var scrolled = $(window).scrollTop()-$('.parallax-wrap-prices-1').offset().top;
            $('#parallax-bg-prices-1').css('top', (0 - (scrolled * .4)) + 'px');
        }
        
        if($('.parallax-wrap-programms-1').length){
            var scrolled = $(window).scrollTop()-$('.parallax-wrap-programms-1').offset().top;
            $('#parallax-bg-programms-1').css('top', (0 - (scrolled * .4)) + 'px');
        }
        
        if($('.parallax-wrap-rooms-1').length){
            var scrolled = $(window).scrollTop()-$('.parallax-wrap-rooms-1').offset().top;
            $('#parallax-bg-rooms-1').css('top', (0 - (scrolled * .4)) + 'px');
        }
        
        if($('.parallax-wrap-rooms-view-1').length){
            var scrolled = $(window).scrollTop()-$('.parallax-wrap-rooms-view-1').offset().top;
            $('#parallax-bg-rooms-view-1').css('top', (0 - (scrolled * .4)) + 'px');
        }
        
      }
    /* ------------------- parallax ------------------ */
    
    
    
    /* mobile menu*/
    $('.js-open-menu').on('click', function(event) {
		event.preventDefault();		
		$('body, .main-content').addClass('menu-open');
		$('.sidebar-inner').fadeIn(300);		
		$('.js-close-menu').on('transitionend webkitTransitionEnd oTransitionEnd', function (event) {
			if (!$('.js-close-menu').hasClass('is-visible') && event.originalEvent.propertyName == 'transform' && $('.main-content').hasClass('menu-open')) {
				$('.js-close-menu').addClass('is-visible');
				console.log(333);
			}
		});
	});
	

	$('.js-close-menu').on('click', function(event) {
		event.preventDefault();
        $('body, .main-content').removeClass('menu-open');
				$('.sidebar-inner').fadeOut(300);	
				console.log(222);
		
		$('.js-close-menu').removeClass('is-visible');
		
		$('.js-close-menu').on('transitionend webkitTransitionEnd oTransitionEnd', function (event) {
			if (!$('.js-close-menu').hasClass('is-visible') && event.originalEvent.propertyName == 'opacity') {
		    $('body, .main-content').removeClass('menu-open');
				$('.sidebar-inner').fadeOut(300);	
				console.log(222);
			}
		});
        
	});
    /* mobile menu*/
    
    
    /*** ABOUT ***/    
    
    /* block-offers images*/
    $('.block-offers-col-image').each(function(index, element) {
        var image=$(this).attr('src');
		$(this).parent('.block-offers-col__image').css('background-image', 'url('+image+')');
		$(this).remove();
    });
    /* block-offers images*/
 
     /* block-offers images*/
    
   
    $('.slider-about-info').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      fade: true,
      asNavFor: '.slider-about-icons'
    });
    $('.slider-about-icons').slick({
      slidesToShow: 7,
      slidesToScroll: 1,
      asNavFor: '.slider-about-info',
      dots: false,
      arrows:false,
      focusOnSelect: true,
        responsive: [
        {
          breakpoint: 1000,
          settings: {
            slidesToShow: 5            
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2
          }
        }
      ]
    });
    
    $('.gallery-show-more').click(function() {
        $(this).parents('.gallery-cols').find('.gallery-cols__item-hidden').show();
        return false;
    })

    
    $('.news-show-more').click(function() {
        $(this).parents('.news-cols').find('.news-cols__item-hidden').show();
        return false;
    })
    
    
    /* WOW animate*/
    
    if(window.innerWidth>767){
        wow = new WOW(
          {
            animateClass: 'animated',
            offset:       200,
            callback:     function(box) {          
            }
          }
        );
        wow.init();  
    }

    /* rooms slider */
    $('.slider-rooms-info').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      fade: true,
      asNavFor: '.slider-rooms-icons'
    });
    $('.slider-rooms-icons').slick({
      slidesToShow: 7,
      slidesToScroll: 1,
      asNavFor: '.slider-rooms-info',
      dots: false,
      arrows:false,
      focusOnSelect: true,
         variableWidth: true,
        responsive: [
        {
          breakpoint: 1000,
          settings: {
            slidesToShow: 5            
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2
          }
        }
      ]
    });
	/* rooms slider */
    
    
    /*room gallery*/
    $(".open-room-gallery").on('click', function() {
  
      $.fancybox.open([
        {
          src  : 'images/image-room-view-01.jpg'
        },
        {
          src  : 'images/image-room-2.jpg'          
        }
          ,
        {
          src  : 'images/image-room-3.jpg'          
        }
          ,
        {
          src  : 'images/image-room-4.jpg'          
        }
      ], {
        margin : [ 40, 0, 104, 0 ],
        infobar : true,
        buttons : true,
        // What buttons should appear in the toolbar
        slideShow  : false,
        fullScreen : false,
        thumbs     : false,
        closeBtn   : true,
        closeClickOutside : true,
        touch : true,
        thumbs : {
            showOnStart : true
        },        
        
            onComplete : function() {
                 $('.fancybox-slide').on('click touchstart',function(event) {
                    if (!$(event.target).closest('.fancybox-placeholder').length) {
                        $.fancybox.close();
                    } 
                })
            }
      });

    });
    
    
    /*Booking window*/
    $('.block-booking-title, .block-booking-close').click(function() {
        $('.block-booking').toggleClass('opened');
        return false;
    })
    
    
});
